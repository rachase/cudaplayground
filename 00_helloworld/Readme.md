Running through simple example as described here:  
https://devblogs.nvidia.com/even-easier-introduction-cuda/

add.cpp - adds two vectors of 1M elements using processor  
add_1_1.cu - adds two vectors of 1M elements using GPU, 1 thread, 1 block  
add_1_256.cu - adds two vectors of 1M elements using GPU, 256 threads, 1 block  
add_n_256.cu - adds two vectors of 1M elements using GPU, 256 thread per n block  

profiles of the cuda programs can be run using 
```
nvprof ./bin/add_1_1
nvprof ./bin/add_1_256 
nvprof ./bin/add_n_256 
```

