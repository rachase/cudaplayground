#include <iostream>
#include <math.h>

// function to add the elements of two arrays
void add(int n, float *x, float *y)
{
  for (int i = 0; i < n; i++)
      y[i] = x[i] + y[i];
}

int main(int argc,char* argv[])
{
  //read in the arguments
  if (argc != 2)
  {
    std::cerr << "You did not feed me arguments, I will die now" << std::endl;
    std::cerr << "Feed me input N where # of elements = 2^N" <<std::endl;
    exit(1);
  }

  unsigned int exp = atoi(argv[1]); 
  if (exp > 62)
  {
    std::cout << "Input too big, using 2^63 elements" << std::endl; 
    exp = 62;
  }
  else
  {
    std::cout << "Using 2^" << exp << " elements" << std::endl;
  }
  

  int N = 1<<exp; // 1M elements

  float *x = new float[N];
  float *y = new float[N];

  // initialize x and y arrays on the host
  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  // Run kernel on 1M elements on the CPU
  add(N, x, y);

  // Check for errors (all values should be 3.0f)
  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = fmax(maxError, fabs(y[i]-3.0f));
  std::cout << "Max error: " << maxError << std::endl;

  // Free memory
  delete [] x;
  delete [] y;

  return 0;
}