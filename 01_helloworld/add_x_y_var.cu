#include <iostream>
#include <math.h>

//https://devblogs.nvidia.com/even-easier-introduction-cuda/

//__global__ tells the CUDA C++ compiler that this is a function that runs on the GPU and can be called from CPU code.
//__global__ functions are called kernels
__global__
// function to add the elements of two arrays
void add(int n, float *x, float *y)
{
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  for (int i = index; i < n; i += stride)
    y[i] = x[i] + y[i];
}

int main(int argc,char* argv[])
{
    //read in the arguments
  if (argc != 4)
  {
    std::cerr << "You did not feed me the right arguments, I will die now" << std::endl;
    std::cerr << "Feed me input N x y where:" << std::endl;
    std::cerr << "N # of elements = 2^N" <<std::endl;
    std::cerr << "x = block size" <<std::endl;
    std::cerr << "y = threads per block" <<std::endl;

    exit(1);
  }

  unsigned int exp = atoi(argv[1]); 
  if (exp > 62)
  {
    std::cout << "Input too big, using 2^63 elements" << std::endl; 
    exp = 62;
  }
  else
  {
    std::cout << "Using 2^" << exp << " elements" << std::endl;
  }
  
  int N = 1<<exp;

  unsigned int blocks = atoi(argv[2]); 
  if (blocks > 4096)
  {
    std::cout << "# blocks too big, using 4096" << std::endl; 
    blocks = 4096;
  }
  else
  {
    std::cout << "Using " << blocks << " blocks" << std::endl;
  }

  unsigned int threads = atoi(argv[3]); 
  if (threads > 4096)
  {
    std::cout << "# threads too big, using 4096" << std::endl; 
    threads = 4096;
  }
  else
  {
    std::cout << "Using " << threads << " threads" << std::endl;
  }


  //allocate memory, traditional c
  //float *x = new float[N];
  //float *y = new float[N];

  // Allocate Unified Memory – accessible from CPU or GPU
  float *x,*y;
  cudaMallocManaged(&x, N*sizeof(float));
  cudaMallocManaged(&y, N*sizeof(float));

  // initialize x and y arrays on the host
  for (int i = 0; i < N; i++) {
    x[i] = 1.0f;
    y[i] = 2.0f;
  }

  // Run kernel on 1M elements on the CPU
  //add(N, x, y);

  // Run the kernel on 1M elements on the GPU
  // add<<<1, 256>>>(N, x, y);

  // int blockSize = blocks;
  // int numBlocks = (N + blockSize - 1) / blockSize;
  // add<<<numBlocks, blockSize>>>(N, x, y);

  add<<<blocks, threads>>>(N, x, y);

  // Wait for GPU to finish before accessing on host
  cudaDeviceSynchronize();

  // Check for errors (all values should be 3.0f)
  float maxError = 0.0f;
  for (int i = 0; i < N; i++)
    maxError = fmax(maxError, fabs(y[i]-3.0f));
  std::cout << "Max error: " << maxError << std::endl;

  // Free memory
  //delete [] x;
  //delete [] y;

  //Cuda Free memory
  cudaFree(x);
  cudaFree(y);

  return 0;
}