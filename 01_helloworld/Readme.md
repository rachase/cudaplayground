Adding variable inputs to example 00_helloworld

add_var N - adds 2 arrays with 2^N elements;  
add_x_y_var N x y - adds 2 arrays with 2^N elements using x blocks and y threads.  

Example of 2^24 elements, on 1 block with 256 threads
```
./bin/add_var 24
./bin/add_x_y_var 24 1 256
```
